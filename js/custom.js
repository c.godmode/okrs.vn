// preloader
window.addEventListener("load", function () {
  const loader = document.querySelector(".loader");
  loader.className += " hidden";
});

// select2
if ($(".new-select2").length) {
  $(".new-select2").select2();
}

// datepicker
if ($(".new-datepicker").length) {
  $(".new-datepicker").datepicker();
}

if ($(".new-add-department").length) {
  $(".new-add-department").on("click", function () {
    $(".new-add-department-container").append(
      '<input type="text" class="form-control new-w-467 mb-3 new-text-gray-3 new-h5" placeholder="Vd: Phòng hành chính">'
    );
  });
}

if ($(".new-add-work-position").length) {
  $(".new-add-work-position").on("click", function () {
    $(".new-add-work-position-container").append(
      '<input type="text" class="form-control new-w-467 mb-3 new-text-gray-3 new-h5" placeholder="Vd: Trưởng phòng kỹ thuật, Nhân viên kinh doanh...">'
    );
  });
}

if ($(".new-add-key-result").length) {
  var i = 1;
  $(".new-add-key-result").on("click", function () {
    i += 1;
    $(".new-add-key-result-container").append(
      `<div class="p-3 border mb-3"
        style="-webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1), 0 1px 5px rgba(0, 0, 0, 0.1);-moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1), 0 1px 5px rgba(0, 0, 0, 0.1);-ms-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1), 0 1px 5px rgba(0, 0, 0, 0.1);-o-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1), 0 1px 5px rgba(0, 0, 0, 0.1);box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1), 0 1px 5px rgba(0, 0, 0, 0.1);">
        <div class="row">
            <div class="col-12">
                <label for="" class="new-text-violet">Key result ${i}</label>
            </div>
            <div class="col-5">
                <input name="lastname" id="lastname" type="text"
                    class="form-control new-w-638 new-h-40 new-text-gray-5"
                    placeholder="Nhập Objecttive của bạn" />
            </div>
            <div class="col-4 d-flex">
                <label for="" class="mb-0 mr-3 new-lh-40 new-h7" style="flex: 0 0 70px;">Mục
                    tiêu</label>
                <input name="lastname" id="lastname" type="text"
                    class="form-control new-h-40 new-text-gray-5" placeholder="000" />
            </div>
            <div class="col-3">
                <div class="new-select new-h-40 position-relative">
                    <select name="location" id="location" class="form-control new-select2">
                        <option value="AL">Chọn đơn vị đo</option>
                        <option value="WY">Nữ</option>
                    </select>
                    <div
                        class="new-w-29 new-h-40 position-absolute new-bg-gray h-100 d-flex new-select__icon">
                        <img src="${require(`./assets/arrow-down.svg`)}" alt="" srcset=""
                            class="m-auto" />
                    </div>
                </div>
            </div>
        </div>
    </div>`
    );
    $(".new-select2").select2();
  });
}

if ($(".new-add-key-result-2").length) {
  var i = 1;
  $(".new-add-key-result-2").on("click", function () {
    i += 1;
    $(".new-add-key-result-2-container").append(
      `<div class="p-3 border mb-3"
        style="-webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1), 0 1px 5px rgba(0, 0, 0, 0.1);-moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1), 0 1px 5px rgba(0, 0, 0, 0.1);-ms-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1), 0 1px 5px rgba(0, 0, 0, 0.1);-o-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1), 0 1px 5px rgba(0, 0, 0, 0.1);box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1), 0 1px 5px rgba(0, 0, 0, 0.1);">
        <div class="row">
            <div class="col-12">
                <label for="" class="new-text-violet">Key result ${i}</label>
            </div>
            <div class="col-6 mb-3">
                <input name="lastname" id="lastname" type="text"
                    class="form-control new-w-638 new-h-40 new-text-gray-5"
                    placeholder="Nhập Objecttive của bạn" />
            </div>
            <div class="col-3 d-flex mb-3">
                <label for="" class="mb-0 mr-3 new-lh-40 new-h7"
                    style="flex: 0 0 70px;">Mục
                    tiêu</label>
                <input name="lastname" id="lastname" type="text"
                    class="form-control new-h-40 new-text-gray-5" placeholder="000" />
            </div>
            <div class="col-3 mb-3">
                <div class="new-select new-h-40 position-relative">
                    <select name="location" id="location"
                        class="form-control new-select2">
                        <option value="AL">Chọn đơn vị đo</option>
                        <option value="WY">Nữ</option>
                    </select>
                    <div
                        class="new-w-29 new-h-40 position-absolute new-bg-gray h-100 d-flex new-select__icon">
                        <img src="${require(`./assets/arrow-down.svg`)}" alt=""
                            srcset="" class="m-auto" />
                    </div>
                </div>
            </div>
            <div class="col-6">
                <label for="">Link kế hoạch</label>
                <input name="lastname" id="lastname" type="text"
                    class="form-control new-w-638 new-h-40 new-text-gray-5" />
            </div>
            <div class="col-6">
                <label for="">Link kết quả</label>
                <input name="lastname" id="lastname" type="text"
                    class="form-control new-w-638 new-h-40 new-text-gray-5" />
            </div>
        </div>
    </div>`
    );
    $(".new-select2").select2();
  });
}

if ($(".add-cross-linking-okr").length) {
  var countCrossLinkingOkr = 0;
  $(".add-cross-linking-okr").on("click", function () {
    if (countCrossLinkingOkr % 2 === 0) {
      $(".add-cross-linking-okr").before(
        `<div class="col-5 mb-3"><input name="lastname" id="lastname" type="text"
              class="form-control new-w-638 new-h-40 new-text-gray-5"
              placeholder="Tìm OKRs liên kết chéo" />
      </div><label class="mb-3 col-2"></label>`
      );
    } else {
      $(".add-cross-linking-okr").before(
        `<div class="col-5">
          <input name="lastname" id="lastname" type="text"
              class="form-control new-w-638 new-h-40 new-text-gray-5"
              placeholder="Tìm OKRs liên kết chéo" />
      </div>`
      );
    }
    countCrossLinkingOkr += 1;
  });
}

if ($(".new-add-work-position").length) {
  $(".new-add-work-position").on("click", function () {
    $(".new-add-work-position-container").append(
      '<input type="text" class="form-control new-w-467 mb-3 new-text-gray-3 new-h5" placeholder="Vd: Trưởng phòng kỹ thuật, Nhân viên kinh doanh...">'
    );
  });
}

if ($("#createProfile").length) {
  $("#createProfile").validate({
    errorPlacement: function (error, element) {
      if (element.hasClass("new-select2")) {
        error.insertAfter(element.next("span"));
      } else {
        error.insertAfter(element);
      }
    },
    rules: {
      lastname: "required",
      firstname: "required",
      birthday: "required",
      email: {
        required: true,
        email: true,
      },
      part: "required",
      department: "required",
      nickname: "required",
      gender: "required",
      phonenumber: "required",
      location: "required",
      bossemail: "required",
    },
    messages: {
      lastname: "Nhập họ.",
      firstname: "Nhập tên.",
      birthday: "Nhập ngày sinh.",
      email: "Sai địa chỉ Email.",
      part: "Chọn bộ phận.",
      department: "Chọn phòng ban.",
      nickname: "Nhập bí danh.",
      gender: "Chọn giới tính.",
      phonenumber: "Nhập số điện thoại.",
      location: "Chọn vị trí.",
      bossemail: "Nhập địa chỉ email sếp.",
    },
  });
}

$(".tree").treegrid();